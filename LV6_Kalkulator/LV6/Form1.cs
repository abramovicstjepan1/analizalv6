﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
//potrebno omoguciti operacije (*, /, +, -) i jos barem 5 dodatnih operacija (omogucio sam 6 dodatnih)
namespace LV6
{
    public partial class Znanst_kalk : Form
    {
        double op1, op2; //ove dvije varijable ovdje deklariramo zato da ne moramo to raditi ponovno za svaku od funkcija 
        public Znanst_kalk()
        {
            InitializeComponent();
        }

        //funkcija za gumb '*'
        private void puta_Click(object sender, EventArgs e)
        {

            if (!double.TryParse(Broj1.Text, out op1))
                MessageBox.Show("Pogrešan unos prvog broja!", "Pogreška!");
            else if (!double.TryParse(Broj2.Text, out op2))
                MessageBox.Show("Pogrešan unos drugog broja!", "Pogreška!");
            else
                Rezultat.Text = (op1 * op2).ToString();
        }

        //funkcija za gumb '/'
        private void podjeljeno_Click(object sender, EventArgs e)
        {

            if (!double.TryParse(Broj1.Text, out op1))
                MessageBox.Show("Pogrešan unos prvog broja!", "Pogreška!");
            else if (!double.TryParse(Broj2.Text, out op2))
                MessageBox.Show("Pogrešan unos drugog broja!", "Pogreška!");
            else
                Rezultat.Text = (op1 / op2).ToString();

        }

        //funkcija za gumb '+'
        private void plus_Click(object sender, EventArgs e)
        {


            if (!double.TryParse(Broj1.Text, out op1))
                MessageBox.Show("Pogrešan unos prvog broja!", "Pogreška!");
            else if (!double.TryParse(Broj2.Text, out op2))
                MessageBox.Show("Pogrešan unos drugog broja!", "Pogreška!");
            else
                Rezultat.Text = (op1 + op2).ToString();
        }

        //funkcija za gumb '-'
        private void minus_Click(object sender, EventArgs e)
        {

            if (!double.TryParse(Broj1.Text, out op1))
                MessageBox.Show("Pogrešan unos prvog broja!", "Pogreška!");
            else if (!double.TryParse(Broj2.Text, out op2))
                MessageBox.Show("Pogrešan unos drugog broja!", "Pogreška!");
            else
                Rezultat.Text = (op1 - op2).ToString();
        }

        //funkcija za gumb 'sin'
        private void sinus_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(Broj1.Text, out op1))
                MessageBox.Show("Pogrešan unos prvog broja!", "Pogreška!");
            else
            {
                Broj2.Clear();
                Rezultat.Text = (Math.Sin(op1 * Math.PI / 180)).ToString();
            }
        }

        //funkcija za gumb 'cos'
        private void kosinus_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(Broj1.Text, out op1))
                MessageBox.Show("Pogrešan unos prvog broja!", "Pogreška!");
            else
            {
                Broj2.Clear();
                Rezultat.Text = (Math.Cos(op1 * Math.PI / 180)).ToString();
            }
        }

        //funkcija za gumb 'sqrt'
        private void korijen_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(Broj1.Text, out op1))
                MessageBox.Show("Pogrešan unos prvog broja!", "Pogreška!");
            else if (op1 <= 0)
                MessageBox.Show("Br1 mora biti veci od 0!");
            else
            {
                Broj2.Clear();
                Rezultat.Text = (Math.Sqrt(op1)).ToString();
            }
        }

        //funkcija za gumb 'ln'
        private void prirodni_log_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(Broj1.Text, out op1))
                MessageBox.Show("Pogrešan unos prvog broja!", "Pogreška!");
            else
            {
                Broj2.Clear();
                Rezultat.Text = (Math.Log(op1)).ToString();
            }
        }

        //funkcija za gumb '^Br2'
        private void eksponent_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(Broj1.Text, out op1))
                MessageBox.Show("Pogrešan unos prvog broja!", "Pogreška!");
            else if (!double.TryParse(Broj2.Text, out op2))
                MessageBox.Show("Pogrešan unos drugog broja!", "Pogreška!");
            else
                Rezultat.Text = (Math.Pow(op1, op2)).ToString();
        }

        //funkcija za gumb 'e^Br1'
        private void e_Exp_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(Broj1.Text, out op1))
                MessageBox.Show("Pogrešan unos prvog broja!", "Pogreška!");

            else
            {
                Broj2.Clear();
                Rezultat.Text = (Math.Exp(op1)).ToString();
            }
        }
    }
}
